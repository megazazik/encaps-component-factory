<a name="0.5.0"></a>
# [0.5.0](https://github.com/megazazik/encaps/compare/v0.4.1...v0.5.0) (2019-05-17)


### Features

* **handlers:** add posibility to set a string instead of a handler ([ecccf1b](https://github.com/megazazik/encaps/commit/ecccf1b))



<a name="0.4.1"></a>
## [0.4.1](https://github.com/megazazik/encaps/compare/v0.4.0...v0.4.1) (2018-12-07)


### Bug Fixes

* **build:** fix build bug ([7a418eb](https://github.com/megazazik/encaps/commit/7a418eb))



<a name="0.4.0"></a>
# [0.4.0](https://github.com/megazazik/encaps/compare/v0.3.2...v0.4.0) (2018-12-07)


### Features

* add effects ([4c84b82](https://github.com/megazazik/encaps/commit/4c84b82))
* add effects method of Builder ([3fe1c64](https://github.com/megazazik/encaps/commit/3fe1c64))
* **actions:** add action creator's types ([cb3acd2](https://github.com/megazazik/encaps/commit/cb3acd2))
* **controller:** add effect method to builder ([1e27cdc](https://github.com/megazazik/encaps/commit/1e27cdc))
* **controller:** change interface ([e08ee4e](https://github.com/megazazik/encaps/commit/e08ee4e))
* **effects:** add select function to effects ([456b4ba](https://github.com/megazazik/encaps/commit/456b4ba))



<a name="0.3.2"></a>
## [0.3.2](https://github.com/megazazik/encaps/compare/v0.3.1...v0.3.2) (2018-09-06)



<a name="0.3.1"></a>
## [0.3.1](https://github.com/megazazik/encaps/compare/v0.3.0...v0.3.1) (2018-07-15)



<a name="0.3.0"></a>
# [0.3.0](https://github.com/megazazik/encaps/compare/v0.2.1...v0.3.0) (2018-07-15)



<a name="0.2.1"></a>
## [0.2.1](https://github.com/megazazik/encaps/compare/v0.2.0...v0.2.1) (2018-03-04)



<a name="0.2.0"></a>
# [0.2.0](https://github.com/megazazik/encaps/compare/v0.1.0...v0.2.0) (2017-11-30)



<a name="0.1.0"></a>
# 0.1.0 (2017-08-24)



